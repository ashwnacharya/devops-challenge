# Solution for the Devops Challenge for the position of Senior Devops Engineer/Devops Manager.

## Prerequisites
Install ansible

```
pip install -r requirements.txt
```

## To bring up the VM
Run the command 

```
vagrant up
```

## What it does.

Vagrant will provison a [Bento Centos 7 box](https://app.vagrantup.com/bento/boxes/centos-7).

The Vagrantfile uses the ansible provisioner to provision the web app.
The web app is run using gunicorn so that it can scale better.
Gunicorn serves the web app at port 5000 inside the VM.

Supervisord manages the gunicorn processes so that it can respawn if killed.

Ngnix reverse proxies to port 5000 and serves the web app at port 80.

The port 80 of the VM is mapped to port 80 of the host.


